const Velocity = require('velocityjs');
const context = require('./context.json');
const fs = require('fs');
const phantom = require('phantom');

const writeHtml = (html) => {
    fs.writeFile("out.html", html, (err) => {
        if (err) {
            return console.log(err);
        }
        console.log("Great success!");
    });
};

const writePdf = (html) => {
    phantom.create()
        .then(ph => ph.createPage())
        .then(page =>
            page.property('content', html)
                .then(() => page.render('out.pdf'))
        )
        .then(console.log("Keep on rolling"))
}

fs.readFile('template.vm', 'utf8', (err, data) => {

    if (err) {
        return console.log(err);
    }

    const html = Velocity.render(data, context);

    writeHtml(html);
    writePdf(html);
});